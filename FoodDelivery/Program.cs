using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using CloudinaryDotNet;
using dotenv.net;
using FoodDelivery.Configurations;
using FoodDelivery.Hubs;
using FoodDelivery.Models;
using FoodDelivery.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

namespace FoodDelivery;

public class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);
        builder.Configuration.AddJsonFile("appsettings.user.json", true);
        builder.Services.AddControllers();
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();
        builder.Host.ConfigureServices(ConfigureServices);
        builder.Host.ConfigureServices(ConfigureSettings);
        builder.Host.ConfigureServices(ConfigureAuth);
        
        // DotEnv.Load(options: new DotEnvOptions(probeForEnv: true));
        // var cloudinary = new Cloudinary(Environment.GetEnvironmentVariable("CLOUDINARY_URL"));
        // cloudinary.Api.Secure = true;
        //
        
        var AllowOrigin = "_bookingFood";
        builder.Services.AddCors(options =>
        {
            options.AddPolicy(AllowOrigin, policy =>
            {
                policy.WithOrigins("*", "http://localhost:3000")
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials();
            });
        });

        var app = builder.Build();

        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }
        else
        {
            app.UseHttpsRedirection();
        }
        app.UseRouting();

        app.UseCors(AllowOrigin);
        app.UseAuthentication();

        app.UseStaticFiles();

        app.UseAuthorization();

        app.UseEndpoints(e =>
        {
            e.MapHub<ChatHub>("/chat");
            e.MapControllers();
            e.MapRazorPages();
        });

        app.Run();
    }

    private static void ConfigureServices(HostBuilderContext context, IServiceCollection services)
    {
        services.AddSignalR();
        services.AddDbContext<FoodDeliveryDbContext>(options =>
        {
            options.UseNpgsql(context.Configuration.GetConnectionString("DbConnectionString"));
        });
        
        services.AddControllers(options =>
        {
            options.OutputFormatters.RemoveType<SystemTextJsonOutputFormatter>();
            options.OutputFormatters.Add(new SystemTextJsonOutputFormatter(
                new JsonSerializerOptions(JsonSerializerDefaults.Web)
                {
                    ReferenceHandler = ReferenceHandler.IgnoreCycles
                }));
        });

        services.AddMvc();
        services.AddRazorPages();
        services.AddScoped<UserService>();
        services.AddScoped<RestaurantService>();
        services.AddScoped<FoodService>();
        services.AddScoped<CategoryService>();
        services.AddScoped<MediaService>();
    }

    private static void ConfigureAuth(HostBuilderContext context, IServiceCollection services)
    {
        services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = context.Configuration["Jwt:Issuer"],
                    ValidAudience = context.Configuration["Jwt:Audience"],
                    IssuerSigningKey =
                        new SymmetricSecurityKey(
                            Encoding.UTF8.GetBytes(context.Configuration["Jwt:Key"] ?? string.Empty))
                };
             });
        services.AddHttpContextAccessor();
    }

    private static void ConfigureSettings(HostBuilderContext context, IServiceCollection services)
    {
        services.Configure<JwtConfigurations>(context.Configuration.GetSection("Jwt"));
    }
}