﻿using FoodDelivery.Dtos;
using FoodDelivery.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FoodDelivery.Services;

public class CategoryService
{
    private readonly FoodDeliveryDbContext _dbContext;
    private readonly UserService _userService;

    public CategoryService(FoodDeliveryDbContext dbContext, UserService userService)
    {
        _dbContext = dbContext;
        _userService = userService;
    }

    public async Task<IActionResult> CreateCategory(CategoryDto categoryDto,CancellationToken cancellationToken)
    {
        var currentUser = await _userService.GetCurrentUser(cancellationToken);
        if (currentUser?.Restaurant is null)
        {
            throw new NullReferenceException("User is Restaurant is null!");
        }
        var category = await _dbContext.CategoryEntities.FirstOrDefaultAsync(
            c => c.RestaurantId == currentUser.Restaurant.Id && c.Name.ToLower() == categoryDto.Name.ToLower(),
            cancellationToken);
        if (category is not null) return new BadRequestResult();
        category = new CategoryEntity()
        {
            Name = categoryDto.Name,
            RestaurantId = currentUser.Restaurant.Id
        };
        _dbContext.Add(category);
        await _dbContext.SaveChangesAsync(cancellationToken);
        return new OkResult();
    }

    public async Task<List<CategoryEntity>> GetCategoriesOfRes(CancellationToken cancellationToken)
    {
        var currentUser = await _userService.GetCurrentUser(cancellationToken);
        var categories = await _dbContext.CategoryEntities
            .AsNoTracking()
            .Where(c => currentUser != null && c.RestaurantId == currentUser.Restaurant.Id)
            .ToListAsync(cancellationToken);
        return categories;
    }

    public async Task<CategoryEntity?> GetCategory(Guid id, CancellationToken cancellationToken)
    {
        if (id == Guid.Empty)
        {
            throw new NullReferenceException("CategoryId Empty!");
        }
        var category = await _dbContext.CategoryEntities
            .Include(x => x.Foods)
            .FirstOrDefaultAsync(c => c.Id == id, cancellationToken);
        if (category == null)
        {
            throw new NullReferenceException("Category is null!");
        }
        return category;
    }

    public async Task<IActionResult> RemoveCategory(Guid id, CancellationToken cancellationToken)
    {
        var currentUser = await _userService.GetCurrentUser(cancellationToken);

        var category =
            await _dbContext.CategoryEntities.FirstOrDefaultAsync(c =>
                currentUser != null && c.RestaurantId == currentUser.Restaurant.Id && c.Id == id, cancellationToken);
        if (category == null)
        {
            throw new NullReferenceException();
        }

        _dbContext.Remove(category);
        await _dbContext.SaveChangesAsync(cancellationToken);
        
        return new OkResult();
    }
}