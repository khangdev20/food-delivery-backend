﻿using System.Net;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using FoodDelivery.Dtos;
using FoodDelivery.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FoodDelivery.Services;

public class FoodService
{
    private readonly FoodDeliveryDbContext _dbContext;
    private readonly UserService _userService;

    public FoodService(FoodDeliveryDbContext dbContext, UserService userService)
    {
        _dbContext = dbContext;
        _userService = userService;
    }

    public async Task<IActionResult> CreateFoodToRes(FoodDto foodDto, CancellationToken cancellationToken)
    {
        var food = await _dbContext.FoodEntities
                .FirstOrDefaultAsync(f =>
                f.CategoryId == foodDto.CategoryId && 
                f.Name.ToLower() == foodDto.Name.ToLower(),
                cancellationToken);
        if (food != null)
        {
            return new BadRequestObjectResult("Food is exist!")
            {
                StatusCode = 402
            };
        }
        var newFood = new FoodEntity()
        {
            Name = foodDto.Name,
            Describe = foodDto.Describe,
            Price = foodDto.Price,
            CategoryId = foodDto.CategoryId,
            Created = DateTime.UtcNow
        };
        _dbContext.Add(newFood);
        await _dbContext.SaveChangesAsync(cancellationToken);
        return new OkResult();
    }

    public async Task<IActionResult> DeleteFood(Guid id, CancellationToken cancellationToken)
    {
        var food = await _dbContext.FoodEntities.FirstOrDefaultAsync(f => f.Id == id, cancellationToken);
        if (food is null)
        {
            throw new NullReferenceException("Food is null!");
        }

        _dbContext.Remove(food);
        await _dbContext.SaveChangesAsync(cancellationToken);
        return new OkResult();
    }
    
}