﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using FoodDelivery.Configurations;
using FoodDelivery.Dtos;
using FoodDelivery.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace FoodDelivery.Services;

public class UserService
{
    private readonly FoodDeliveryDbContext _dbContext;
    private readonly IOptions<JwtConfigurations> _jwtOptions;
    private readonly IHttpContextAccessor _httpContextAccessor;

    public UserService(FoodDeliveryDbContext dbContext, IOptions<JwtConfigurations> jwtOptions,  IHttpContextAccessor httpContextAccessor)
    {
        _dbContext = dbContext;
        _jwtOptions = jwtOptions;
        _httpContextAccessor = httpContextAccessor;
    }

    [Obsolete("Obsolete")]
    private static string GetMd5(string str)
    {
        MD5 md5 = new MD5CryptoServiceProvider();
        byte[] fromData = Encoding.UTF8.GetBytes(str);
        byte[] targetData = md5.ComputeHash(fromData);
        string byte2String = null;

        for (int i = 0; i < targetData.Length; i++)
        {
            byte2String += targetData[i].ToString("x2");
        }

        return byte2String;
    }

    public string GenerateToken(UserEntity user)
    {
        var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtOptions.Value.Key));
        var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

        var claims = new[]
        {
            new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
            new Claim(ClaimTypes.Name, user.Name),
            new Claim(ClaimTypes.Email, user.Email),
            new Claim(ClaimTypes.MobilePhone, user.Phone),
            new Claim(ClaimTypes.Role, user.Roles)
        };

        var token = new JwtSecurityToken(
            _jwtOptions.Value.Issuer,
            _jwtOptions.Value.Audience,
            claims,
            expires: DateTime.UtcNow.AddHours(1),
            signingCredentials: credentials
        );
        return new JwtSecurityTokenHandler().WriteToken(token);
    }

    public async Task<UserEntity?> Authenticate(LoginDto loginDto, CancellationToken cancellationToken)
    {
        var user = await _dbContext.UserEntities.FirstOrDefaultAsync(
            u => u.Email.ToLower().Trim() == loginDto.Email.ToLower().Trim() &&
                 u.Password == GetMd5(loginDto.Password),
            cancellationToken
        );
        if (user is null) throw new NullReferenceException("Cannot find user");
        return user;
    }

    public async Task<UserEntity?> GetCurrentUser(CancellationToken cancellationToken)
    {
        var httpContext = _httpContextAccessor.HttpContext;
        if (httpContext is null)
        {
            throw new NullReferenceException("HttpContext is null");
        }
        if (httpContext.User.Identity is not ClaimsIdentity identity)
            throw new NullReferenceException("HttpContext is null");
        var userClaims = identity.Claims;
        var value = userClaims.FirstOrDefault(i => i.Type == ClaimTypes.NameIdentifier)?.Value;
        if (value is null)
        {
            throw new NullReferenceException("User is null");
        }
        var userId = Guid.Parse(value);
        var user = await _dbContext.UserEntities
            .Include(x => x.Restaurant)
            .FirstOrDefaultAsync(u => u.Id == userId, cancellationToken);
        if (user is null) throw new NullReferenceException("Cannot find user");
        return user;
    }

    public async Task<UserEntity> CreateUser(UserDto userDto, CancellationToken cancellationToken)
    {
        var userValidate = await _dbContext.UserEntities.FirstOrDefaultAsync(u => u.Email == userDto.Email, cancellationToken);
        if (userValidate != null) throw new NullReferenceException("User is exist");

        if (userDto.Email == "" && userDto.Name == "" && userDto.Password == "" && userDto.Phone == "")
        {
            throw new NullReferenceException("User info is empty!");
        }

        var newUser = new UserEntity()
        {
            Name = userDto.Name,
            Email = userDto.Email,
            Roles = "Res",
            Phone = userDto.Phone,
            Password = GetMd5(userDto.Password)
        };
        _dbContext.Add(newUser);
        await _dbContext.SaveChangesAsync(cancellationToken);
        return newUser;
    }

    public async Task<List<UserEntity>> GetUsers(CancellationToken cancellationToken)
    {
        var users = await _dbContext.UserEntities
            .Include(u => u.Restaurant)
            .ToListAsync(cancellationToken);
        return users;
    }

    public async Task<IActionResult> RemoveUser(Guid id, CancellationToken cancellationToken)
    {
        if (id == Guid.Empty)
        {
            return new BadRequestObjectResult("UserId is Empty!")
            {
                StatusCode = 411
            };
        }
        var user = await _dbContext.UserEntities.FirstOrDefaultAsync(u => u.Id == id, cancellationToken);
        if (user is null)
        {
            throw new NullReferenceException("User is null");
        }
        _dbContext.Remove(user);
        await _dbContext.SaveChangesAsync(cancellationToken);
        return new OkResult();
    }
}