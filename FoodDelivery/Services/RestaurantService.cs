﻿using FoodDelivery.Dtos;
using FoodDelivery.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FoodDelivery.Services;

public class RestaurantService
{
    private readonly FoodDeliveryDbContext _dbContext;
    private readonly UserService _userService;

    public RestaurantService(FoodDeliveryDbContext dbContext, UserService userService)
    {
        _dbContext = dbContext;
        _userService = userService;
    }

    public async Task<IActionResult> CreateRestaurant(RestaurantDto restaurantDto, CancellationToken cancellationToken)
    {
        var currentUser = await _userService.GetCurrentUser(cancellationToken);
        if (currentUser != null)
        {
            var newRes = new RestaurantEntity()
            {
                Name = restaurantDto.Name,
                Address = restaurantDto.Address,
                UserId = currentUser.Id
            };
            _dbContext.Add(newRes);
        }

        await _dbContext.SaveChangesAsync(cancellationToken);
        return new OkResult();
    }

    public async Task<IActionResult> RemoveRes(Guid id, CancellationToken cancellationToken)
    {
        if (id == Guid.Empty)
        {
            throw new NullReferenceException("ResId is empty!");
        }
        var res = await _dbContext.RestaurantEntities.FirstOrDefaultAsync(r => r.Id == id, cancellationToken);
        if (res == null) return new BadRequestResult();
        _dbContext.Remove(res);
        await _dbContext.SaveChangesAsync(cancellationToken);
        return new OkResult();
    }

    public async Task<List<RestaurantEntity>> GetAllRestaurant(CancellationToken cancellationToken)
    {
        var restaurants = await _dbContext.RestaurantEntities
            .Include(x => x.User)
            .ToListAsync(cancellationToken);

        return restaurants;
    }

    public async Task<RestaurantEntity?> GetRestaurant(Guid id, CancellationToken cancellationToken)
    {
        if (id == Guid.Empty)
        {
            throw new NullReferenceException("RestaurantId is empty!");
        }
        var restaurant = await _dbContext.RestaurantEntities
            .Include(x => x.Categories)
            .FirstOrDefaultAsync(r => r.Id == id, cancellationToken);
        return restaurant;
    }


}