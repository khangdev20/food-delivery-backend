﻿using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Mvc;

namespace FoodDelivery.Services;

public class MediaService
{
    private readonly FoodDeliveryDbContext _dbContext;
    private string CLOUD = "dfvskv6og";
    private string API_KEY = "941598865492711";
    private string API_SECRECT = "w1fyYguEvn4xiaZE5roE1tNq-ck";
    private Cloudinary? cloudinary;

    public MediaService(FoodDeliveryDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    private static async Task<string> GetPathFile(IFormFile file)
    {
        var filePath = Path.GetTempFileName();
        await using var stream = File.Create(filePath);
        await file.CopyToAsync(stream);
        return filePath;
    }

    private void CloudinaryStorage()
    {
        var account = new Account(
            CLOUD,
            API_KEY,
            API_SECRECT);
        cloudinary = new Cloudinary(account);
    }

    public async Task<IActionResult> UploadImage(IFormFile file)
    {
        CloudinaryStorage();
        var filePath = await GetPathFile(file);
        var uploadImage = new ImageUploadParams()
        {
            File = new FileDescription(filePath),
            PublicId = $"images/foods/{Guid.NewGuid()}",
            Overwrite = false
        };
        var result = cloudinary?.Upload(uploadImage);
        return new OkObjectResult(new
        {
            url = result?.Url,
            statusCode = result?.StatusCode
        });
    }
}