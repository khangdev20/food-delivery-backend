﻿using FoodDelivery.Dtos;
using FoodDelivery.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
 
namespace FoodDelivery.Controllers;

[Authorize]
[Controller]
[Route("api/[controller]")]
public class FoodController : ControllerBase
{
    private readonly RestaurantService _restaurantService;
    private readonly FoodService _foodService;

    public FoodController(RestaurantService restaurantService, FoodService foodService)
    {
        _restaurantService = restaurantService;
        _foodService = foodService;
    }
    
    
    [AllowAnonymous]
    [HttpPost]
    [Route("")]
    public async Task<IActionResult> CreateFood([FromBody] FoodDto foodDto, CancellationToken cancellationToken)
    {
        var food = await _foodService.CreateFoodToRes(foodDto, cancellationToken);
        return Ok(food);
    }

    [HttpDelete]
    [Route("{id}")]
    public async Task<IActionResult> DeleteFood(Guid id, CancellationToken cancellationToken)
    {
        var result = await _foodService.DeleteFood(id, cancellationToken);
        return Ok(result);
    }
}