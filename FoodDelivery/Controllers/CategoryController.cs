﻿using FoodDelivery.Dtos;
using FoodDelivery.Models;
using FoodDelivery.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FoodDelivery.Controllers;


[Authorize]
[Controller]
[Route("api/[controller]")]
public class CategoryController : ControllerBase
{
    private readonly CategoryService _categoryService;

    public CategoryController(CategoryService categoryService)
    {
        _categoryService = categoryService;
    }

    [HttpPost]
    [Route("")]
    public async Task<IActionResult> CreateCategory([FromBody] CategoryDto categoryDto, CancellationToken cancellationToken)
    {
        var newCate = await _categoryService.CreateCategory(categoryDto, cancellationToken);
        return Ok(newCate);
    }

    [HttpGet]
    [Route("category-restaurant")]
    public async Task<List<CategoryEntity>> GetCategoryOfRes(CancellationToken cancellationToken)
    {
        var listCategory = await _categoryService.GetCategoriesOfRes(cancellationToken);
        return listCategory;
    }

    [HttpGet]
    [Route("{id}")]
    public async Task<CategoryEntity?> GetCategory(Guid id, CancellationToken cancellationToken)
    {
        var category = await _categoryService.GetCategory(id, cancellationToken);
        return category;
    }

    [HttpDelete]
    [Route("{id}")]
    public async Task<IActionResult> RemoveCategory(Guid id, CancellationToken cancellationToken)
    {
        var category = await _categoryService.RemoveCategory(id, cancellationToken);
        return Ok(category);
    }

}