﻿using FoodDelivery.Dtos;
using FoodDelivery.Models;
using FoodDelivery.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FoodDelivery.Controllers;

[Authorize]
[Controller]
[Route("api/[controller]")]
public class RestaurantController : ControllerBase
{
    private readonly RestaurantService _restaurantService;

    public RestaurantController(RestaurantService restaurantService)
    {
        _restaurantService = restaurantService;
    }

    [AllowAnonymous]
    [HttpGet]
    [Route("")]
    public async Task<List<RestaurantEntity>> GetRestaurants(CancellationToken cancellationToken)
    {
        var res = await _restaurantService.GetAllRestaurant(cancellationToken);
        return res;
    }

    [AllowAnonymous]
    [HttpGet]
    [Route("{id}")]
    public async Task<RestaurantEntity?> GetRestaurant(Guid id, CancellationToken cancellationToken)
    {
        var restaurant = await _restaurantService.GetRestaurant(id, cancellationToken);
        return restaurant;
    }

    [HttpPost]
    [Route("")]
    public async Task<IActionResult> CreateNewRes([FromBody]RestaurantDto restaurantDto, CancellationToken cancellationToken)
    {
        var newRes = await _restaurantService.CreateRestaurant(restaurantDto, cancellationToken);
        return Ok(newRes);
    }

    [AllowAnonymous]
    [HttpDelete]
    [Route("{id}")]
    public async Task<IActionResult> RemoveRestaurant(Guid id, CancellationToken cancellationToken)
    {
        var removeRes = await _restaurantService.RemoveRes(id, cancellationToken);
        return Ok(removeRes);
    }

}