﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using FoodDelivery.Dtos;
using FoodDelivery.Models;
using FoodDelivery.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FoodDelivery.Controllers;


[Authorize]
[Controller]
[Route("api/[controller]")]
public class UserController : ControllerBase
{
    private readonly FoodDeliveryDbContext _dbContext;
    private readonly UserService _userService;
    private readonly MediaService _mediaService;

    public UserController(FoodDeliveryDbContext dbContext, UserService userService, MediaService mediaService)
    {
        _dbContext = dbContext;
        _userService = userService;
        _mediaService = mediaService;
    }

    [HttpGet]
    [Route("profile")]
    public async Task<Object> GetUserProfile(CancellationToken cancellationToken)
    {
        var user = await _userService.GetCurrentUser(cancellationToken);
        if (user == null)
        {
            throw new NullReferenceException("");
        }
        var userFormat = new UserEntity()
        {
            Id = user.Id,
            Name = user.Name,
            Email = user.Email,
            Phone = user.Phone,
            Roles = user.Roles,
            Restaurant = user.Restaurant
        };
        
        return userFormat ;
    }
   
    [AllowAnonymous]
    [HttpPost]
    [Route("register")]
    public async Task<UserEntity> Register([FromBody]UserDto userDto, CancellationToken cancellationToken)
    {
        var user = await _userService.CreateUser(userDto, cancellationToken);
        return user;
    }

    [AllowAnonymous]
    [HttpPost]
    [Route("login")]
    public async Task<IActionResult> Login([FromBody] LoginDto loginDto, CancellationToken cancellationToken)
    {
        try
        {
            var user = await _userService.Authenticate(loginDto, cancellationToken);
            if (user is null)
            {
                return new BadRequestObjectResult("User is null");
            }
            var token = _userService.GenerateToken(user);
            return new OkObjectResult(new
            {
                token = token
            });
        }
        catch (NullReferenceException e)
        {
            return new BadRequestObjectResult($"Cannot find user {e}");
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    [AllowAnonymous]
    [HttpGet]
    [Route("")]
    public async Task<List<UserEntity>> GetUsers(CancellationToken cancellationToken)
    {
        var users = await _userService.GetUsers(cancellationToken);
        return users;
    }
    
    [AllowAnonymous]
    [HttpDelete]
    [Route("{id}")]
    public async Task<IActionResult> RemoveUser(Guid id, CancellationToken cancellationToken)
    {
        var userRemove = await _userService.RemoveUser(id, cancellationToken);
        return Ok(userRemove);
    }

    [AllowAnonymous]
    [HttpPost]
    [Route("uploads")]
    public async Task<IActionResult> UploadDemo(IFormFile file)
    {
        var result = await _mediaService.UploadImage(file);
        return new OkObjectResult(result);
    }
}
