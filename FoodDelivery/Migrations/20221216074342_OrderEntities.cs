﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FoodDelivery.Migrations
{
    /// <inheritdoc />
    public partial class OrderEntities : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderEntity_RestaurantEntities_RestaurantId",
                table: "OrderEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_OrderEntity_UserEntities_UserId",
                table: "OrderEntity");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OrderEntity",
                table: "OrderEntity");

            migrationBuilder.RenameTable(
                name: "OrderEntity",
                newName: "OrderEntities");

            migrationBuilder.RenameIndex(
                name: "IX_OrderEntity_UserId",
                table: "OrderEntities",
                newName: "IX_OrderEntities_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_OrderEntity_RestaurantId",
                table: "OrderEntities",
                newName: "IX_OrderEntities_RestaurantId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_OrderEntities",
                table: "OrderEntities",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderEntities_RestaurantEntities_RestaurantId",
                table: "OrderEntities",
                column: "RestaurantId",
                principalTable: "RestaurantEntities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_OrderEntities_UserEntities_UserId",
                table: "OrderEntities",
                column: "UserId",
                principalTable: "UserEntities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderEntities_RestaurantEntities_RestaurantId",
                table: "OrderEntities");

            migrationBuilder.DropForeignKey(
                name: "FK_OrderEntities_UserEntities_UserId",
                table: "OrderEntities");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OrderEntities",
                table: "OrderEntities");

            migrationBuilder.RenameTable(
                name: "OrderEntities",
                newName: "OrderEntity");

            migrationBuilder.RenameIndex(
                name: "IX_OrderEntities_UserId",
                table: "OrderEntity",
                newName: "IX_OrderEntity_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_OrderEntities_RestaurantId",
                table: "OrderEntity",
                newName: "IX_OrderEntity_RestaurantId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_OrderEntity",
                table: "OrderEntity",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderEntity_RestaurantEntities_RestaurantId",
                table: "OrderEntity",
                column: "RestaurantId",
                principalTable: "RestaurantEntities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_OrderEntity_UserEntities_UserId",
                table: "OrderEntity",
                column: "UserId",
                principalTable: "UserEntities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
