﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FoodDelivery.Migrations
{
    /// <inheritdoc />
    public partial class RestaurantIdIntoCategory : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CategoryEntities_RestaurantEntity_RestaurantEntityId",
                table: "CategoryEntities");

            migrationBuilder.DropIndex(
                name: "IX_CategoryEntities_RestaurantEntityId",
                table: "CategoryEntities");

            migrationBuilder.DropColumn(
                name: "RestaurantEntityId",
                table: "CategoryEntities");

            migrationBuilder.AddColumn<Guid>(
                name: "RestaurantId",
                table: "CategoryEntities",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_CategoryEntities_RestaurantId",
                table: "CategoryEntities",
                column: "RestaurantId");

            migrationBuilder.AddForeignKey(
                name: "FK_CategoryEntities_RestaurantEntity_RestaurantId",
                table: "CategoryEntities",
                column: "RestaurantId",
                principalTable: "RestaurantEntity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CategoryEntities_RestaurantEntity_RestaurantId",
                table: "CategoryEntities");

            migrationBuilder.DropIndex(
                name: "IX_CategoryEntities_RestaurantId",
                table: "CategoryEntities");

            migrationBuilder.DropColumn(
                name: "RestaurantId",
                table: "CategoryEntities");

            migrationBuilder.AddColumn<Guid>(
                name: "RestaurantEntityId",
                table: "CategoryEntities",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_CategoryEntities_RestaurantEntityId",
                table: "CategoryEntities",
                column: "RestaurantEntityId");

            migrationBuilder.AddForeignKey(
                name: "FK_CategoryEntities_RestaurantEntity_RestaurantEntityId",
                table: "CategoryEntities",
                column: "RestaurantEntityId",
                principalTable: "RestaurantEntity",
                principalColumn: "Id");
        }
    }
}
