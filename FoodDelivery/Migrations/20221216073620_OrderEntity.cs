﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FoodDelivery.Migrations
{
    /// <inheritdoc />
    public partial class OrderEntity : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CategoryEntities_RestaurantEntity_RestaurantId",
                table: "CategoryEntities");

            migrationBuilder.DropForeignKey(
                name: "FK_RestaurantEntity_UserEntities_UserId",
                table: "RestaurantEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionEntities_RestaurantEntity_RestaurantId",
                table: "TransactionEntities");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RestaurantEntity",
                table: "RestaurantEntity");

            migrationBuilder.RenameTable(
                name: "RestaurantEntity",
                newName: "RestaurantEntities");

            migrationBuilder.RenameIndex(
                name: "IX_RestaurantEntity_UserId",
                table: "RestaurantEntities",
                newName: "IX_RestaurantEntities_UserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RestaurantEntities",
                table: "RestaurantEntities",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CategoryEntities_RestaurantEntities_RestaurantId",
                table: "CategoryEntities",
                column: "RestaurantId",
                principalTable: "RestaurantEntities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RestaurantEntities_UserEntities_UserId",
                table: "RestaurantEntities",
                column: "UserId",
                principalTable: "UserEntities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionEntities_RestaurantEntities_RestaurantId",
                table: "TransactionEntities",
                column: "RestaurantId",
                principalTable: "RestaurantEntities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CategoryEntities_RestaurantEntities_RestaurantId",
                table: "CategoryEntities");

            migrationBuilder.DropForeignKey(
                name: "FK_RestaurantEntities_UserEntities_UserId",
                table: "RestaurantEntities");

            migrationBuilder.DropForeignKey(
                name: "FK_TransactionEntities_RestaurantEntities_RestaurantId",
                table: "TransactionEntities");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RestaurantEntities",
                table: "RestaurantEntities");

            migrationBuilder.RenameTable(
                name: "RestaurantEntities",
                newName: "RestaurantEntity");

            migrationBuilder.RenameIndex(
                name: "IX_RestaurantEntities_UserId",
                table: "RestaurantEntity",
                newName: "IX_RestaurantEntity_UserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RestaurantEntity",
                table: "RestaurantEntity",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CategoryEntities_RestaurantEntity_RestaurantId",
                table: "CategoryEntities",
                column: "RestaurantId",
                principalTable: "RestaurantEntity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RestaurantEntity_UserEntities_UserId",
                table: "RestaurantEntity",
                column: "UserId",
                principalTable: "UserEntities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionEntities_RestaurantEntity_RestaurantId",
                table: "TransactionEntities",
                column: "RestaurantId",
                principalTable: "RestaurantEntity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
