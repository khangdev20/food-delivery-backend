﻿using System.ComponentModel.DataAnnotations;

namespace FoodDelivery.Models;

public class ModelsAbstract
{
    [Key]
    public Guid Id { get; set; }
    public DateTime Created { get; set; }
    public DateTime Modified { get; set; }
}