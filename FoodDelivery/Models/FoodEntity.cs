﻿namespace FoodDelivery.Models;

public class FoodEntity : ModelsAbstract
{
    public string Name { get; set; }
    public string Describe { get; set; }
    public decimal Price { get; set; }
    public Guid CategoryId { get; set; }
    public CategoryEntity Category { get; set; }
}