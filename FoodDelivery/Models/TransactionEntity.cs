﻿namespace FoodDelivery.Models;

public class TransactionEntity : ModelsAbstract
{
    public decimal Price { get; set; }
    public Guid UserId { get; set; }
    public UserEntity User { get; set; }
    public Guid RestaurantId { get; set; }
    public RestaurantEntity Restaurant { get; set; }
}