﻿namespace FoodDelivery.Models;

public class CategoryEntity : ModelsAbstract
{ 
    public string Name { get; set; }
    public List<FoodEntity> Foods { get; set; }
    public Guid RestaurantId { get; set; }
    public RestaurantEntity Restaurant { get; set; }
}