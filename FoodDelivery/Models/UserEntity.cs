﻿namespace FoodDelivery.Models;

public class UserEntity : ModelsAbstract
{
    public string Name { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }
    public string Phone { get; set; }
    public string Roles { get; set; }
    public RestaurantEntity Restaurant { get; set; }
    public List<OrderEntity> Orders { get; set; }
}