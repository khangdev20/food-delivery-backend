﻿namespace FoodDelivery.Models;

public class RestaurantEntity : ModelsAbstract
{
    public string Name { get; set; }
    public string Address { get; set; }
    public Guid UserId { get; set; }
    public UserEntity User { get; set; }
    public List<TransactionEntity> Transactions { get; set; }
    public List<CategoryEntity> Categories { get; set; }
    public List<OrderEntity> Orders { get; set; }
}