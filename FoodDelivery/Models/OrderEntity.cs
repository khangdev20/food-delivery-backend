﻿namespace FoodDelivery.Models;

public class OrderEntity : ModelsAbstract
{
    public Guid UserId { get; set; }
    public UserEntity User { get; set; }
    public Guid RestaurantId { get; set; }
    public RestaurantEntity Restaurant { get; set; }
}