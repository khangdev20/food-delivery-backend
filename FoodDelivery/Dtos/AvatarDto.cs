﻿using System.ComponentModel.DataAnnotations;
using System.Net;

namespace FoodDelivery.Dtos;

public class AvatarDto
{
    [Display(Name = "File")]
    public IFormFile file { get; set; }
}