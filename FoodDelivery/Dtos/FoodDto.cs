﻿using System.ComponentModel.DataAnnotations;

namespace FoodDelivery.Dtos;

public class FoodDto
{
    public string Name { get; set; }
    public string Describe { get; set; }
    public decimal Price { get; set; }
    public Guid CategoryId { get; set; }
}