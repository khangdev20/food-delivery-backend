﻿namespace FoodDelivery.Dtos;

public class CategoryDto
{
    public string Name { get; set; }
}