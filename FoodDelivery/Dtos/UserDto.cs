﻿namespace FoodDelivery.Dtos;

public class UserDto
{
    public string Name { get; set; }
    public string Password { get; set; }
    public string Phone { get; set; }
    public string Email { get; set; }
}