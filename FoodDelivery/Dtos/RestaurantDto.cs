﻿namespace FoodDelivery.Dtos;

public class RestaurantDto
{
    public string Name { get; set; }
    public string Address { get; set; }
}