﻿namespace FoodDelivery.Configurations;

public class JwtConfigurations
{
    public string Key { get; set; }
    public string Issuer { get; set; }
    public string Audience { get; set; }
}