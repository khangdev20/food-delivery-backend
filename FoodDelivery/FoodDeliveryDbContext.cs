﻿using FoodDelivery.Models;
using Microsoft.EntityFrameworkCore;

namespace FoodDelivery;

public class FoodDeliveryDbContext : DbContext
{
    public FoodDeliveryDbContext(DbContextOptions<FoodDeliveryDbContext> options) : base(options)
    {
    }

    public DbSet<UserEntity> UserEntities { get; set; }
    public DbSet<CategoryEntity> CategoryEntities { get; set; }
    public DbSet<FoodEntity> FoodEntities { get; set; }
    public DbSet<TransactionEntity> TransactionEntities { get; set; }
    public DbSet<RestaurantEntity> RestaurantEntities { get; set; }
    public DbSet<OrderEntity> OrderEntities { get; set; }
}