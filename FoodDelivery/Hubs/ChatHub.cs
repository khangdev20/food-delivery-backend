﻿using System.Diagnostics;
using FoodDelivery.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;

namespace FoodDelivery.Hubs;

public class ChatHub : Hub
{
    private readonly UserService _userService;
    public ChatHub(UserService userService)
    {
        _userService = userService;
    }
    public async Task SendMessage(string user, string message)
    {
        await Clients.All.SendAsync("ReceiveMessage", user, message);
    }

    
    //
    // public async Task<Task> OnConnectedAsync(CancellationToken cancellationToken)
    // {
    //     var currentUser = await _userService.GetCurrentUser(cancellationToken);
    //     Debug.Assert(currentUser != null, nameof(currentUser) + " != null");
    //     await Groups.AddToGroupAsync(Context.ConnectionId, currentUser.Name, cancellationToken);
    //     return base.OnConnectedAsync();
    // }
    
    // public async Task SendMessage(string message)
    // {
    //     CancellationToken cancellationToken = default;
    //     var currentUser = await  _userService.GetCurrentUser(cancellationToken);
    //     if (currentUser != null) await Clients.All.SendAsync("ReceiveMessage", currentUser.Name, message,cancellationToken);
    // }
    //
    // public Task SendMessageToGroup(string sender, string receiver, string message)
    // {
    //     return Clients.Group(receiver).SendAsync("ReceiveMessage", sender, message);
    // }
}